# Обертка для работы с официальным API Wildberries на Python

[https://openapi.wb.ru/](https://openapi.wb.ru/)

## Установка

```sh
pip install -U git+https://gitlab.com/fomch/wildberries_official_api_wrapper
```

## Пример использования

```python
from wildberries_official_api_wrapper import StatisticAPI

statistic_api = StatisticAPI('<token>')
print(statistic_api.get_incomes('2024-01-01'))
```

## Поддерживаемые разделы API

- [ ] Контент (Только https://openapi.wildberries.ru/content/api/ru/#tag/Prosmotr)
- [x] Цены и скидки
- [x] Поставки
- [ ] Маркетплейс (Только https://openapi.wildberries.ru/marketplace/api/ru/#tag/Sklady)
- [x] Статистика
- [x] Аналитика
- [x] Продвижение
- [x] Рекомендации
- [x] Вопросы и Отзывы
- [x] Тарифы
- [x] Чат с покупателями
- [x] Возвраты покупателями
