from setuptools import setup, find_packages

setup(
    name="wildberries_official_api_wrapper",
    version="1.22",
    packages=find_packages(),
    description="WB official API wrapper",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    author="Fomchenkov Vyacheslav",
    author_email="fomchenkov.dev@gmail.com",
    url="https://gitlab.com/fomch/wildberries_official_api_wrapper",
    install_requires=[
        "requests",
        "deprecated",
    ],
)
