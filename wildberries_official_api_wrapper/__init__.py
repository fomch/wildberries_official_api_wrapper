from .analytic import *
from .content import *
from .feedbacks_questions import *
from .marketplace import *
from .prices import *
from .promotion import *
from .recommendations import *
from .statistic import *
from .tariffs import *
from .buyers_chat import *
from .returns import *
from .supplies import *


__version__ = "1.22"
__author__ = "Fomchenkov Vyacheslav"
__email__ = "fomchenkov.dev@gmail.com"
__description__ = "Wildberries Official API Wrapper"
__url__ = "https://gitlab.com/fomch/wildberries_official_api_wrapper"
