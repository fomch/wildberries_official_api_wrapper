import requests


class AnalyticsAPI:
    """
    Класс для работы с аналитикой

    https://openapi.wildberries.ru/analytics/api/ru/
    """

    def __init__(self, wb_token):
        self.wb_token = wb_token
        self.headers = {
            "Authorization": self.wb_token,
            "Content type": "application/json",
        }

    def nm_report(self, nmIDs, begin, end, page=1):
        """
        Получение статистики КТ за выбранный период, по nmID/предметам/брендам/тегам

        https://openapi.wildberries.ru/analytics/api/ru/#tag/Voronka-prodazh/paths/~1content~1v1~1analytics~1nm-report~1detail/post
        """

        data = {
            "nmIDs": nmIDs,
            "period": {
                "begin": begin,
                "end": end,
            },
            "page": page,
        }
        url = "https://seller-analytics-api.wildberries.ru/api/v2/nm-report/detail"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def nm_report_grouped(self, nmIDs, begin, end, page=1):
        """
        Получение статистики КТ за период, сгруппированный по предметам, брендам и тегам

        https://openapi.wildberries.ru/analytics/api/ru/#tag/Voronka-prodazh/paths/~1content~1v1~1analytics~1nm-report~1grouped/post
        """

        data = {
            "nmIDs": nmIDs,
            "period": {
                "begin": begin,
                "end": end,
            },
            "page": page,
        }
        url = "https://seller-analytics-api.wildberries.ru/api/v2/nm-report/grouped"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def nm_report_history(
        self, nmIDs, begin, end, aggregationLevel, timezone="Europe/Moscow"
    ):
        """
        Получение статистики КТ по дням/неделям/месяцам по выбранным nmID

        https://openapi.wb.ru/analytics/api/ru/#tag/Voronka-prodazh/paths/~1api~1v2~1nm-report~1detail~1history/post
        """

        data = {
            "nmIDs": nmIDs,
            "period": {
                "begin": begin,
                "end": end,
            },
            "timezone": timezone,
            "aggregationLevel": aggregationLevel,
        }
        url = "https://seller-analytics-api.wildberries.ru/api/v2/nm-report/detail/history"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def nm_report_history_grouped(
        self, nmIDs, begin, end, aggregationLevel, timezone="Europe/Moscow"
    ):
        """
        Получение статистики КТ по дням/неделям/месяцам за период, сгруппированный по предметам, брендам и тегам

        https://openapi.wb.ru/analytics/api/ru/#tag/Voronka-prodazh/paths/~1api~1v2~1nm-report~1grouped~1history/post
        """

        data = {
            "objectIDs": nmIDs,
            "period": {
                "begin": begin,
                "end": end,
            },
            "timezone": timezone,
            "aggregationLevel": aggregationLevel,
        }
        url = "https://seller-analytics-api.wildberries.ru/api/v2/nm-report/grouped/history"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def excise_report(self, dateFrom, dateTo, countries=[]):
        """
        Отчёт по товарам с обязательной маркировкой

        https://openapi.wb.ru/analytics/api/ru/#tag/Tovary-s-obyazatelnoj-markirovkoj/paths/~1api~1v1~1analytics~1excise-report/post
        """

        params = {
            "dateFrom": dateFrom,
            "dateTo": dateTo,
        }
        data = {}
        if countries:
            data["countries"] = countries
        url = "https://seller-analytics-api.wildberries.ru/api/v1/analytics/excise-report"
        res = requests.post(url, headers=self.headers, params=params, json=data)
        return res.json()

    def acceptance_report(self, dateFrom, dateTo):
        """
        Платная приемка

        https://openapi.wildberries.ru/analytics/api/ru/#tag/Platnaya-priyomka/paths/~1api~1v1~1analytics~1acceptance-report/get
        """

        params = {
            "dateFrom": dateFrom,
            "dateTo": dateTo,
        }
        url = "https://seller-analytics-api.wildberries.ru/api/v1/analytics/acceptance-report"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def antifraud_details(self, date=None):
        """
        Отчет по удержаниям. Самовыкупы

        https://openapi.wb.ru/analytics/api/ru/#tag/Otchyoty-po-uderzhaniyam/paths/~1api~1v1~1analytics~1antifraud-details/get
        """

        params = {}
        if date:
            params["date"] = date
        url = "https://seller-analytics-api.wildberries.ru/api/v1/analytics/antifraud-details"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()
    
    def incorrect_attachments(self, dateFrom, dateTo):
        """
        Отчет по удержаниям. Подмена товара

        https://openapi.wildberries.ru/analytics/api/ru/#tag/Otchyoty-po-uderzhaniyam/paths/~1api~1v1~1analytics~1incorrect-attachments/get
        """

        params = {
            'dateFrom': dateFrom,
            'dateTo': dateTo,
        }
        url = "https://seller-analytics-api.wildberries.ru/api/v1/analytics/incorrect-attachments"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def storage_coefficient(self, date):
        """
        Отчет по удержаниям. Коэффициент логистики и хранения

        https://openapi.wildberries.ru/analytics/api/ru/#tag/Otchyoty-po-uderzhaniyam/paths/~1api~1v1~1analytics~1storage-coefficient/get
        """

        params = {
            "date": date,
        }
        url = "https://seller-analytics-api.wildberries.ru/api/v1/analytics/storage-coefficient"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def goods_labeling(self, dateFrom, dateTo):
        """
        Отчет по удержаниям. Маркировка товара

        https://openapi.wb.ru/analytics/api/ru/#tag/Otchyoty-po-uderzhaniyam/paths/~1api~1v1~1analytics~1goods-labeling/get
        """

        params = {
            "dateFrom": dateFrom,
            "dateTo": dateTo,
        }
        url = "https://seller-analytics-api.wildberries.ru/api/v1/analytics/goods-labeling"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def characteristics_change(self, dateFrom, dateTo):
        """
        Отчет по удержаниям. Смена характеристик

        https://openapi.wb.ru/analytics/api/ru/#tag/Otchyoty-po-uderzhaniyam/paths/~1api~1v1~1analytics~1characteristics-change/get
        """

        params = {
            "dateFrom": dateFrom,
            "dateTo": dateTo,
        }
        url = "https://seller-analytics-api.wildberries.ru/api/v1/analytics/characteristics-change"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()


class PaidStorageAPI:
    """
    Класс для работы с платным хранением

    https://openapi.wb.ru/analytics/api/ru/#tag/Platnoe-hranenie
    """

    def __init__(self, wb_token):
        self.wb_token = wb_token
        self.headers = {
            "Authorization": self.wb_token,
            "Content type": "application/json",
        }

    def create(self, dateFrom, dateTo):
        """
        Создать отчёт

        https://openapi.wb.ru/analytics/api/ru/#tag/Platnoe-hranenie/paths/~1api~1v1~1paid_storage/get
        """

        params = {
            "dateFrom": dateFrom,
            "dateTo": dateTo,
        }
        url = "https://seller-analytics-api.wildberries.ru/api/v1/paid_storage"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def status(self, task_id):
        """
        Проверить статус

        https://openapi.wb.ru/analytics/api/ru/#tag/Platnoe-hranenie/paths/~1api~1v1~1paid_storage~1tasks~1%7Btask_id%7D~1status/get
        """

        url = f"https://seller-analytics-api.wildberries.ru/api/v1/paid_storage/tasks/{task_id}/status"
        res = requests.get(url, headers=self.headers)
        return res.json()

    def download(self, task_id):
        """
        Получить отчёт

        https://openapi.wb.ru/analytics/api/ru/#tag/Platnoe-hranenie/paths/~1api~1v1~1paid_storage~1tasks~1%7Btask_id%7D~1download/get
        """

        url = f"https://seller-analytics-api.wildberries.ru/api/v1/paid_storage/tasks/{task_id}/download"
        res = requests.get(url, headers=self.headers)
        return res.json()
