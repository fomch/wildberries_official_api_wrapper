import requests


class BuyersChat:
    """
    Класс для работы с чатом с покупателями

    https://openapi.wildberries.ru/buyers-chat/api/ru/
    """

    def __init__(self, wb_token):
        self.wb_token = wb_token
        self.headers = {
            "Authorization": self.wb_token,
            "Content type": "application/json",
        }

    def seller_chats(self):
        """
        Список чатов

        https://openapi.wildberries.ru/buyers-chat/api/ru/#tag/Chat-s-pokupatelyami/paths/~1api~1v1~1seller~1chats/get
        """

        url = "https://buyer-chat-api.wildberries.ru/api/v1/seller/chats"
        res = requests.get(url, headers=self.headers)
        return res.json()

    def seller_events(self, next=None):
        """
        События чатов

        https://openapi.wildberries.ru/buyers-chat/api/ru/#tag/Chat-s-pokupatelyami/paths/~1api~1v1~1seller~1events/get
        """

        params = {}
        if next:
            params["next"] = next
        url = "https://buyer-chat-api.wildberries.ru/api/v1/seller/events"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def seller_message(self, replySign, message=None, file=None):
        """
        Отправить сообщение

        https://openapi.wildberries.ru/buyers-chat/api/ru/#tag/Chat-s-pokupatelyami/paths/~1api~1v1~1seller~1message/post
        """

        data = {
            "replySign": replySign,
        }
        if message:
            data["message"] = message
        files = {}
        if file:
            files["file"] = file
        url = "https://buyer-chat-api.wildberries.ru/api/v1/seller/message"
        res = requests.post(url, headers=self.headers, json=data, files=files)
        return res.json()
