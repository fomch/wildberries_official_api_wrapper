import requests


class CardsAPI:
    """
    Класс для работы с карточками товаров

    https://openapi.wildberries.ru/content/api/ru/#tag/Prosmotr
    """

    def __init__(self, wb_token):
        self.wb_token = wb_token
        self.headers = {
            "Authorization": self.wb_token,
            "Content type": "application/json",
        }

    def get_cards_list(self, sort_ascending=False, filter=None, cursor=None):
        """
        Список номенклатур (НМ)

        https://openapi.wildberries.ru/content/api/ru/#tag/Prosmotr/paths/~1content~1v2~1get~1cards~1list/post
        """

        data = {
            "settings": {
                "sort": {"ascending": sort_ascending},
            }
        }
        if filter:
            data["settings"]["filter"] = filter
        if cursor:
            data["settings"]["cursor"] = cursor
        url = "https://suppliers-api.wildberries.ru/content/v2/get/cards/list"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def cards_error_list(self):
        """
        Список несозданных номенклатур (НМ) с ошибками

        https://openapi.wildberries.ru/content/api/ru/#tag/Prosmotr/paths/~1content~1v2~1cards~1error~1list/get
        """

        url = "https://suppliers-api.wildberries.ru/content/v2/cards/error/list"
        res = requests.get(url, headers=self.headers)
        return res.json()

    def cards_limits(self):
        """
        Лимиты по КТ

        https://openapi.wildberries.ru/content/api/ru/#tag/Prosmotr/paths/~1content~1v2~1cards~1limits/get
        """

        url = "https://suppliers-api.wildberries.ru/content/v2/cards/limits"
        res = requests.get(url, headers=self.headers)
        return res.json()
