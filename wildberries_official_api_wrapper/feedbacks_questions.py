import requests


class QuestionsAPI:
    """
    Класс для работы с вопросами

    https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Voprosy
    """

    def __init__(self, wb_token):
        self.wb_token = wb_token
        self.headers = {
            "Authorization": self.wb_token,
            "Content type": "application/json",
        }

    def questions_count_unanswered(self):
        """
        Неотвеченные вопросы

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Voprosy/paths/~1api~1v1~1questions~1count-unanswered/get
        """

        url = "https://feedbacks-api.wildberries.ru/api/v1/questions/count-unanswered"
        res = requests.get(url, headers=self.headers)
        return res.json()

    def new_feedbacks_questions(self):
        """
        Непросмотренные отзывы и вопросы

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Voprosy/paths/~1api~1v1~1new-feedbacks-questions/get
        """

        url = "https://feedbacks-api.wildberries.ru/api/v1/new-feedbacks-questions"
        res = requests.get(url, headers=self.headers)
        return res.json()

    def questions_products_rating(self):
        """
        Часто спрашиваемые товары

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Voprosy/paths/~1api~1v1~1questions~1products~1rating/get
        """

        url = "https://feedbacks-api.wildberries.ru/api/v1/questions/products/rating"
        res = requests.get(url, headers=self.headers)
        return res.json()

    def get_questions(
        self,
        isAnswered=False,
        take=1000,
        skip=0,
        nmId=None,
        order=None,
        dateFrom=None,
        dateTo=None,
    ):
        """
        Список вопросов

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Voprosy/paths/~1api~1v1~1questions/get
        """

        params = {
            "isAnswered": isAnswered,
            "take": take,
            "skip": skip,
        }
        if nmId:
            params["nmId"] = nmId
        if order:
            params["order"] = order
        if dateFrom:
            params["dateFrom"] = dateFrom
        if dateTo:
            params["dateTo"] = dateTo
        url = "https://feedbacks-api.wildberries.ru/api/v1/questions"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def patch_questions(self, id, wasViewed=None, answer_text=None, state=None):
        """
        Работа с вопросами

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Voprosy/paths/~1api~1v1~1questions/patch
        """

        data = {
            "id": id,
        }
        if wasViewed:
            data["wasViewed"] = wasViewed
        if answer_text:
            data["answer"] = {"text": answer_text}
        if state:
            data["state"] = state
        url = "https://feedbacks-api.wildberries.ru/api/v1/questions"
        res = requests.patch(url, headers=self.headers, json=data)
        return res.json()

    def questions_report(self, isAnswered=False):
        """
        Получение вопросов в формате XLSX

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Voprosy/paths/~1api~1v1~1questions~1report/get
        """

        params = {
            "isAnswered": isAnswered,
        }
        url = "https://feedbacks-api.wildberries.ru/api/v1/questions/report"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def questions_count(self, dateFrom=None, dateTo=None, isAnswered=None):
        """
        Количество вопросов

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Voprosy/paths/~1api~1v1~1questions~1count/get
        """

        params = {}
        if dateFrom:
            params["dateFrom"] = dateFrom
        if dateTo:
            params["dateTo"] = dateTo
        if isAnswered:
            params["isAnswered"] = isAnswered
        url = "https://feedbacks-api.wildberries.ru/api/v1/questions/count"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def question(self, id):
        """
        Получить вопрос по Id

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Voprosy/paths/~1api~1v1~1question/get
        """

        params = {
            "id": id,
        }
        url = "https://feedbacks-api.wildberries.ru/api/v1/question"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()


class FeedbacksAPI:
    """
    Класс для работы с отзывами

    https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Otzyvy
    """

    def __init__(self, wb_token):
        self.wb_token = wb_token
        self.headers = {
            "Authorization": self.wb_token,
            "Content type": "application/json",
        }

    def feedbacks_count_unanswered(self):
        """
        Необработанные отзывы

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Otzyvy/paths/~1api~1v1~1feedbacks~1count-unanswered/get
        """

        url = "https://feedbacks-api.wildberries.ru/api/v1/feedbacks/count-unanswered"
        res = requests.get(url, headers=self.headers)
        return res.json()

    def parent_subjects(self):
        """
        Родительские категории товаров

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Otzyvy/paths/~1api~1v1~1parent-subjects/get
        """

        url = "https://feedbacks-api.wildberries.ru/api/v1/parent-subjects"
        res = requests.get(url, headers=self.headers)
        return res.json()

    def feedbacks_products_rating(self, subjectId):
        """
        Средняя оценка товаров по родительской категории

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Otzyvy/paths/~1api~1v1~1feedbacks~1products~1rating/get
        """

        params = {
            "subjectId": subjectId,
        }
        url = "https://feedbacks-api.wildberries.ru/api/v1/feedbacks/products/rating"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def feedbacks_products_rating_top(self, subjectId):
        """
        Товары с наибольшей и наименьшей средней оценкой по родительской категории

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Otzyvy/paths/~1api~1v1~1feedbacks~1products~1rating~1top/get
        """

        params = {
            "subjectId": subjectId,
        }
        url = "https://feedbacks-api.wildberries.ru/api/v1/feedbacks/products/rating/top"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def get_feedbacks(
        self,
        isAnswered=False,
        take=1000,
        skip=0,
        nmId=None,
        order=None,
        dateFrom=None,
        dateTo=None,
    ):
        """
        Список отзывов

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Otzyvy/paths/~1api~1v1~1feedbacks/get
        """

        params = {
            "isAnswered": isAnswered,
            "take": take,
            "skip": skip,
        }
        if nmId:
            params["nmId"] = nmId
        if order:
            params["order"] = order
        if dateFrom:
            params["dateFrom"] = dateFrom
        if dateTo:
            params["dateTo"] = dateTo
        url = "https://feedbacks-api.wildberries.ru/api/v1/feedbacks"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def patch_feedbacks(
        self,
        id,
        wasViewed=None,
        text=None,
        state=None,
        supplierFeedbackValuation=None,
        supplierProductValuation=None,
    ):
        """
        Работа с отзывом

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Otzyvy/paths/~1api~1v1~1feedbacks/patch
        """

        data = {
            "id": id,
        }
        if wasViewed:
            data["wasViewed"] = wasViewed
        if text:
            data["text"] = text
        if state:
            data["state"] = state
        if supplierFeedbackValuation:
            data["supplierFeedbackValuation"] = supplierFeedbackValuation
        if supplierProductValuation:
            data["supplierProductValuation"] = supplierProductValuation
        url = "https://feedbacks-api.wildberries.ru/api/v1/feedbacks"
        res = requests.patch(url, headers=self.headers, json=data)
        return res.json()

    def supplier_valuations(self, x_locale="ru"):
        """
        Получить список оценок

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Otzyvy/paths/~1api~1v1~1supplier-valuations/get
        """

        headers = self.headers.copy()
        headers["X-Locale"] = x_locale
        url = "https://feedbacks-api.wildberries.ru/api/v1/supplier-valuations"
        res = requests.get(url, headers=headers)
        return res.json()

    def feedbacks_order_return(self, feedbackId):
        """
        Возврат товара по ID отзыва

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Otzyvy/paths/~1api~1v1~1feedbacks~1order~1return/post
        """

        data = {
            "feedbackId": feedbackId,
        }
        url = "https://feedbacks-api.wildberries.ru/api/v1/feedbacks/order/return"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def feedbacks_report(
        self, isAnswered, skip=None, dateFrom=None, dateTo=None, order=None
    ):
        """
        Получение отзывов в формате XLSX

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Otzyvy/paths/~1api~1v1~1feedbacks~1report/get
        """

        params = {
            "isAnswered": isAnswered,
        }
        if skip:
            params["skip"] = skip
        if dateFrom:
            params["dateFrom"] = dateFrom
        if dateTo:
            params["dateTo"] = dateTo
        if order:
            params["order"] = order
        url = "https://feedbacks-api.wildberries.ru/api/v1/feedbacks/report"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def feedbacks_archive(self, take, skip, nmId=None, order=None):
        """
        Список архивных отзывов

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Otzyvy/paths/~1api~1v1~1feedbacks~1archive/get
        """

        params = {
            "take": take,
            "skip": skip,
        }
        if nmId:
            params["nmId"] = nmId
        if order:
            params["order"] = order
        url = "https://feedbacks-api.wildberries.ru/api/v1/feedbacks/archive"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def product_rating(self, nmId):
        """
        Средняя оценка товара по артикулу WB

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Otzyvy/paths/~1api~1v1~1feedbacks~1products~1rating~1nmid/get
        """

        params = {
            "nmId": nmId,
        }
        url = "https://feedbacks-api.wildberries.ru/api/v1/feedbacks/products/rating/nmid"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def feedbacks_count(self, dateFrom, dateTo, isAnswered):
        """
        Количество отзывов

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Otzyvy/paths/~1api~1v1~1feedbacks~1count/get
        """

        params = {}
        if dateFrom:
            params["dateFrom"] = dateFrom
        if dateTo:
            params["dateTo"] = dateTo
        if isAnswered:
            params["isAnswered"] = isAnswered
        url = "https://feedbacks-api.wildberries.ru/api/v1/feedbacks/count"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def feedback(self, id):
        """
        Получить отзыв по Id

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Otzyvy/paths/~1api~1v1~1feedback/get
        """

        params = {
            "id": id,
        }
        url = "https://feedbacks-api.wildberries.ru/api/v1/feedback"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()


class TemplatesAPI:
    """
    Класс для работы с шаблонами для вопросов и отзывов

    https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Shablony-dlya-voprosov-i-otzyvov
    """

    def __init__(self, wb_token):
        self.wb_token = wb_token
        self.headers = {
            "Authorization": self.wb_token,
            "Content type": "application/json",
        }

    def get_templates(self, templateType):
        """
        Получить шаблоны ответов

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Shablony-dlya-voprosov-i-otzyvov/paths/~1api~1v1~1templates/get
        """

        params = {
            "templateType": templateType,
        }
        url = "https://feedbacks-api.wildberries.ru/api/v1/templates"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def create_template(self, name, templateType, text):
        """
        Создать шаблон

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Shablony-dlya-voprosov-i-otzyvov/paths/~1api~1v1~1templates/post
        """

        data = {
            "name": name,
            "templateType": templateType,
            "text": text,
        }
        url = "https://feedbacks-api.wildberries.ru/api/v1/templates"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def edit_template(self, name, templateID, text):
        """
        Редактировать шаблон

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Shablony-dlya-voprosov-i-otzyvov/paths/~1api~1v1~1templates/patch
        """

        data = {
            "name": name,
            "templateID": templateID,
            "text": text,
        }
        url = "https://feedbacks-api.wildberries.ru/api/v1/templates"
        res = requests.patch(url, headers=self.headers, json=data)
        return res.json()

    def delete_template(self, templateID):
        """
        Удалить шаблон

        https://openapi.wildberries.ru/feedbacks-questions/api/ru/#tag/Shablony-dlya-voprosov-i-otzyvov/paths/~1api~1v1~1templates/delete
        """

        data = {
            "templateID": templateID,
        }
        url = "https://feedbacks-api.wildberries.ru/api/v1/templates"
        res = requests.delete(url, headers=self.headers, json=data)
        return res.json()
