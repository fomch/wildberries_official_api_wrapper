import requests


class OfficesAPI:
    """
    Класс для работы со складами

    https://openapi.wildberries.ru/marketplace/api/ru/#tag/Sklady
    """

    def __init__(self, wb_token):
        self.wb_token = wb_token
        self.headers = {
            "Authorization": self.wb_token,
            "Content type": "application/json",
        }

    def offices(self):
        """
        Получить список складов WB

        https://openapi.wildberries.ru/marketplace/api/ru/#tag/Sklady/paths/~1api~1v3~1offices/get
        """

        url = "https://suppliers-api.wildberries.ru/api/v3/offices"
        res = requests.get(url, headers=self.headers)
        return res.json()

    def warehouses(self):
        """
        Получить список складов продавца

        https://openapi.wildberries.ru/marketplace/api/ru/#tag/Sklady/paths/~1api~1v3~1warehouses/get
        """

        url = "https://suppliers-api.wildberries.ru/api/v3/warehouses"
        res = requests.get(url, headers=self.headers)
        return res.json()

    def warehouse_create(self, name, officeId):
        """
        Создать склад продавца

        https://openapi.wildberries.ru/marketplace/api/ru/#tag/Sklady/paths/~1api~1v3~1warehouses/post
        """

        data = {
            "name": name,
            "officeId": officeId,
        }
        url = "https://suppliers-api.wildberries.ru/api/v3/warehouses"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def warehouse_update(self, warehouseId, name, officeId):
        """
        Обновить склад

        https://openapi.wildberries.ru/marketplace/api/ru/#tag/Sklady/paths/~1api~1v3~1warehouses~1%7BwarehouseId%7D/put
        """

        data = {
            "name": name,
            "officeId": officeId,
        }
        url = f"https://suppliers-api.wildberries.ru/api/v3/warehouses/{warehouseId}"
        res = requests.put(url, headers=self.headers, json=data)
        return res.json()

    def warehouse_delete(self, warehouseId):
        """
        Удалить склад продавца

        https://openapi.wildberries.ru/marketplace/api/ru/#tag/Sklady/paths/~1api~1v3~1warehouses~1%7BwarehouseId%7D/delete
        """

        url = f"https://suppliers-api.wildberries.ru/api/v3/warehouses/{warehouseId}"
        res = requests.delete(url, headers=self.headers)
        return res.json()
