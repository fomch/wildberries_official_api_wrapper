import requests


class DiscountsPricesAPI:
    """
    Класс для работы с ценами

    https://openapi.wb.ru/prices/api/ru/#tag/Ceny
    """

    def __init__(self, wb_token):
        self.wb_token = wb_token
        self.headers = {
            "Authorization": self.wb_token,
            "Content type": "application/json",
        }

    def upload_task(self, data_arr):
        """
        Установить цены и скидки

        https://openapi.wildberries.ru/prices/api/ru/#tag/Ustanovka-cen-i-skidok/paths/~1api~1v2~1upload~1task/post
        """

        data = {
            "data": data_arr,
        }
        url = "https://discounts-prices-api.wb.ru/api/v2/upload/task"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def upload_task_size(self, data_arr):
        """
        Установить цены для размеров

        https://openapi.wildberries.ru/prices/api/ru/#tag/Ustanovka-cen-i-skidok/paths/~1api~1v2~1upload~1task~1size/post
        """

        data = {
            "data": data_arr,
        }
        url = "https://discounts-prices-api.wb.ru/api/v2/upload/task/size"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def history_tasks(self, uploadID):
        """
        Состояние обработанной загрузки

        https://openapi.wildberries.ru/prices/api/ru/#tag/Sostoyaniya-zagruzok/paths/~1api~1v2~1history~1tasks/get
        """

        params = {
            "uploadID": uploadID,
        }
        url = "https://discounts-prices-api.wb.ru/api/v2/history/tasks"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def history_goods_tasks(self, limit, uploadID, offset=0):
        """
        Детализация обработанной загрузки

        https://openapi.wildberries.ru/prices/api/ru/#tag/Sostoyaniya-zagruzok/paths/~1api~1v2~1history~1goods~1task/get
        """

        params = {
            "limit": limit,
            "uploadID": uploadID,
            "offset": offset,
        }
        url = "https://discounts-prices-api.wb.ru/api/v2/history/goods/task"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def buffer_tasks(self, uploadID):
        """
        Состояние необработанной загрузки

        https://openapi.wildberries.ru/prices/api/ru/#tag/Sostoyaniya-zagruzok/paths/~1api~1v2~1buffer~1tasks/get
        """

        params = {
            "uploadID": uploadID,
        }
        url = "https://discounts-prices-api.wb.ru/api/v2/buffer/tasks"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def buffer_goods_task(self, limit, uploadID, offset=0):
        """
        Детализация необработанной загрузки

        https://openapi.wildberries.ru/prices/api/ru/#tag/Sostoyaniya-zagruzok/paths/~1api~1v2~1buffer~1goods~1task/get
        """

        params = {
            "limit": limit,
            "uploadID": uploadID,
            "offset": offset,
        }
        url = "https://discounts-prices-api.wb.ru/api/v2/buffer/goods/task"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def list_goods_filter(self, limit, offset=0, filterNmID=None):
        """
        Получить товары

        https://openapi.wildberries.ru/prices/api/ru/#tag/Spiski-tovarov/paths/~1api~1v2~1list~1goods~1filter/get
        """

        params = {
            "limit": limit,
            "offset": offset,
        }
        if filterNmID:
            params["filterNmID"] = filterNmID
        url = "https://discounts-prices-api.wb.ru/api/v2/list/goods/filter"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def list_goods_size_nm(self, limit, nmID, offset=0):
        """
        Получить размеры товара

        https://openapi.wildberries.ru/prices/api/ru/#tag/Spiski-tovarov/paths/~1api~1v2~1list~1goods~1size~1nm/get
        """

        params = {
            "limit": limit,
            "nmID": nmID,
            "offset": offset,
        }
        url = "https://discounts-prices-api.wb.ru/api/v2/list/goods/size/nm"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()
