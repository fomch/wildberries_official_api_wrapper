import requests


class AdvertAPI:
    """
    Работа с официальными методами API продвижения

    https://openapi.wildberries.ru/promotion/api/ru/#tag/Prodvizhenie
    """

    def __init__(self, WB_TOKEN):
        self.WB_TOKEN = WB_TOKEN
        self.headers = {
            "Authorization": self.WB_TOKEN,
        }

    def save_ad(self, type, name, subjectId, sum, btype, on_pause, nms, cpm):
        """
        Создать автоматическую кампанию

        https://openapi.wb.ru/promotion/api/ru/#tag/Prodvizhenie/paths/~1adv~1v1~1save-ad/post
        """

        data = {
            "type": type,
            "name": name,
            "subjectId": subjectId,
            "sum": sum,
            "btype": btype,
            "on_pause": on_pause,
            "nms": nms,
            "cpm": cpm,
        }
        url = "https://advert-api.wb.ru/adv/v1/save-ad"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def seacat_save_ad(self, nms, campaignName=None):
        """
        Создать кампанию Поиск + Каталог

        https://openapi.wb.ru/promotion/api/ru/#tag/Prodvizhenie/paths/~1adv~1v2~1seacat~1save-ad/post
        """

        data = {
            "nms": nms,
        }
        if campaignName:
            data["campaignName"] = campaignName
        url = "https://advert-api.wildberries.ru/adv/v2/seacat/save-ad"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def delete(self, id):
        """
        Удаление кампании

        https://openapi.wb.ru/promotion/api/ru/#tag/Prodvizhenie/paths/~1adv~1v0~1delete/get
        """

        params = {
            "id": id,
        }
        url = "https://advert-api.wb.ru/adv/v0/delete"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def promotion_count(self):
        """
        Списки кампаний

        https://openapi.wb.ru/promotion/api/ru/#tag/Prodvizhenie/paths/~1adv~1v1~1promotion~1count/get
        """

        url = "https://advert-api.wb.ru/adv/v1/promotion/count"
        res = requests.get(url, headers=self.headers)
        return res.json()

    def promotion_adverts(
        self, status=None, type=None, order=None, direction=None, adverts_arr=None
    ):
        """
        Информация о кампаниях

        https://openapi.wb.ru/promotion/api/ru/#tag/Prodvizhenie/paths/~1adv~1v1~1promotion~1adverts/post
        """

        params = {}
        if status:
            params["status"] = status
        if type:
            params["type"] = type
        if order:
            params["order"] = order
        if direction:
            params["direction"] = direction

        data = {}
        if adverts_arr:
            data = adverts_arr

        url = "https://advert-api.wb.ru/adv/v1/promotion/adverts"
        res = requests.post(url, headers=self.headers, json=data, params=params)
        return res.json()

    def get_cpm(self, type, param):
        """
        Список ставок

        https://openapi.wb.ru/promotion/api/ru/#tag/Stavki/paths/~1adv~1v0~1cpm/get
        """

        params = {
            "type": type,
            "param": param,
        }
        url = "https://advert-api.wb.ru/adv/v0/cpm"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def edit_cpm(self, advertId, type, cpm, param, instrument=None):
        """
        Изменение ставки у кампании

        https://openapi.wb.ru/promotion/api/ru/#tag/Stavki/paths/~1adv~1v0~1cpm/post
        """

        data = {
            "advertId": advertId,
            "type": type,
            "cpm": cpm,
            "param": param,
        }
        if instrument:
            data["instrument"] = instrument
        url = "https://advert-api.wb.ru/adv/v0/cpm"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def allcpm(self, type, param):
        """
        Список ставок по типу размещения кампании

        https://openapi.wb.ru/promotion/api/ru/#tag/Stavki/paths/~1adv~1v0~1allcpm/post
        """

        params = {
            "type": type,
        }
        data = {
            "param": param,
        }
        url = "https://advert-api.wb.ru/adv/v0/allcpm"
        res = requests.post(url, headers=self.headers, json=data, params=params)
        return res.json()

    def adv_start(self, id):
        """
        Запуск кампании

        https://openapi.wb.ru/promotion/api/ru/#tag/Aktivnost-kampanii/paths/~1adv~1v0~1start/get
        """

        params = {
            "id": id,
        }
        url = "https://advert-api.wb.ru/adv/v0/start"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def adv_pause(self, id):
        """
        Пауза кампании

        https://openapi.wb.ru/promotion/api/ru/#tag/Aktivnost-kampanii/paths/~1adv~1v0~1pause/get
        """

        params = {
            "id": id,
        }
        url = "https://advert-api.wb.ru/adv/v0/pause"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def adv_stop(self, id):
        """
        Завершение кампании

        https://openapi.wb.ru/promotion/api/ru/#tag/Aktivnost-kampanii/paths/~1adv~1v0~1stop/get
        """

        params = {
            "id": id,
        }
        url = "https://advert-api.wb.ru/adv/v0/stop"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def balance(self):
        """
        Баланс

        https://openapi.wb.ru/promotion/api/ru/#tag/Finansy/paths/~1adv~1v1~1balance/get
        """

        url = "https://advert-api.wb.ru/adv/v1/balance"
        res = requests.get(url, headers=self.headers)
        return res.json()

    def budget(self, id):
        """
        Бюджет кампании

        https://openapi.wb.ru/promotion/api/ru/#tag/Finansy/paths/~1adv~1v1~1budget/get
        """

        params = {
            "id": id,
        }
        url = "https://advert-api.wb.ru/adv/v1/budget"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def adv_budget_deposit(self, id, sum, type, _return):
        """
        Пополнение бюджета кампании

        https://openapi.wb.ru/promotion/api/ru/#tag/Finansy/paths/~1adv~1v1~1budget~1deposit/post
        """

        params = {
            "id": id,
        }
        data = {
            "sum": sum,
            "type": type,
            "return": _return,
        }
        url = "https://advert-api.wb.ru/adv/v1/budget/deposit"
        res = requests.post(url, headers=self.headers, json=data, params=params)
        return res.json()

    def adv_upd_intervals(self):
        """
        Получение месячных интервалов для истории затрат

        https://openapi.wb.ru/promotion/api/ru/#tag/Finansy/paths/~1adv~1v1~1upd~1intervals/get
        """

        url = "https://advert-api.wb.ru/adv/v1/upd/intervals"
        res = requests.get(url, headers=self.headers)
        return res.json()

    def adv_upd(self, date_from, date_to):
        """
        Получение истории затрат

        https://openapi.wildberries.ru/promotion/api/ru/#tag/Finansy/paths/~1adv~1v1~1upd/get
        """

        params = {
            "from": date_from,
            "to": date_to,
        }
        url = "https://advert-api.wb.ru/adv/v1/upd"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def adv_payments(self, date_from, date_to):
        """
        Получение истории пополнений счета

        https://openapi.wb.ru/promotion/api/ru/#tag/Finansy/paths/~1adv~1v1~1payments/get
        """

        params = {
            "from": date_from,
            "to": date_to,
        }
        url = "https://advert-api.wb.ru/adv/v1/payments"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def adv_rename(self, advertId, name):
        """
        Переименование кампании

        https://openapi.wb.ru/promotion/api/ru/#tag/Upravlenie-obshimi-parametrami-kampanii/paths/~1adv~1v0~1rename/post
        """

        data = {
            "advertId": advertId,
            "name": name,
        }
        url = "https://advert-api.wb.ru/adv/v0/rename"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def search_supplier_subjects(self):
        """
        Список предметов для кампании в поиске

        https://openapi.wb.ru/promotion/api/ru/#tag/Upravlenie-parametrami-kampanii-v-poiske-i-poisk-+-katalog/paths/~1adv~1v1~1search~1supplier-subjects/get
        """

        url = "https://advert-api.wb.ru/adv/v1/search/supplier-subjects"
        res = requests.get(url, headers=self.headers)
        return res.json()

    def search_supplier_products(self, subject=None):
        """
        Список товаров для кампании в поиске

        https://openapi.wb.ru/promotion/api/ru/#tag/Upravlenie-parametrami-kampanii-v-poiske-i-poisk-+-katalog/paths/~1adv~1v1~1search~1supplier-products/get
        """

        params = {}
        if subject:
            params["subject"] = subject
        url = "https://advert-api.wb.ru/adv/v1/search/supplier-products"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def adv_active(self, id, subjectId, status):
        """
        Изменение активности предметной группы

        https://openapi.wb.ru/promotion/api/ru/#tag/Upravlenie-parametrami-kampanii-v-poiske-i-poisk-+-katalog/paths/~1adv~1v0~1active/get
        """

        params = {
            "id": id,
            "subjectId": subjectId,
            "status": status,
        }
        url = "https://advert-api.wb.ru/adv/v0/active"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def get_search_set_plus(self, id, fixed=None):
        """
        Управление активностью фиксированных фраз

        https://openapi.wb.ru/promotion/api/ru/#tag/Upravlenie-parametrami-kampanii-v-poiske-i-poisk-+-katalog/paths/~1adv~1v1~1search~1set-plus/get
        """

        params = {
            "id": id,
        }
        if fixed:
            params["fixed"] = fixed
        url = "https://advert-api.wb.ru/adv/v1/search/set-plus"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def set_search_set_plus(self, id, pause):
        """
        Установка/удаление фиксированных фраз

        https://openapi.wb.ru/promotion/api/ru/#tag/Upravlenie-parametrami-kampanii-v-poiske-i-poisk-+-katalog/paths/~1adv~1v1~1search~1set-plus/post
        """

        params = {
            "id": id,
        }
        data = {
            "pause": pause,
        }
        url = "https://advert-api.wb.ru/adv/v1/search/set-plus"
        res = requests.post(url, headers=self.headers, json=data, params=params)
        return res.json()

    def search_set_phrase(self, id, phrase):
        """
        Установка/удаление минус-фраз фразового соответствия

        https://openapi.wb.ru/promotion/api/ru/#tag/Upravlenie-parametrami-kampanii-v-poiske-i-poisk-+-katalog/paths/~1adv~1v1~1search~1set-phrase/post
        """

        params = {
            "id": id,
        }
        data = {
            "phrase": phrase,
        }
        url = "https://advert-api.wb.ru/adv/v1/search/set-phrase"
        res = requests.post(url, headers=self.headers, json=data, params=params)
        return res.json()

    def search_set_strong(self, id, strong):
        """
        Установка/удаление минус-фраз точного соответствия

        https://openapi.wb.ru/promotion/api/ru/#tag/Upravlenie-parametrami-kampanii-v-poiske-i-poisk-+-katalog/paths/~1adv~1v1~1search~1set-strong/post
        """

        params = {
            "id": id,
        }
        data = {
            "strong": strong,
        }
        url = "https://advert-api.wb.ru/adv/v1/search/set-strong"
        res = requests.post(url, headers=self.headers, json=data, params=params)
        return res.json()

    def search_set_excluded(self, id, excluded):
        """
        Установка/удаление минус-фраз из поиска

        https://openapi.wb.ru/promotion/api/ru/#tag/Upravlenie-parametrami-kampanii-v-poiske-i-poisk-+-katalog/paths/~1adv~1v1~1search~1set-excluded/post
        """

        params = {
            "id": id,
        }
        data = {
            "excluded": excluded,
        }
        url = "https://advert-api.wb.ru/adv/v1/search/set-excluded"
        res = requests.post(url, headers=self.headers, json=data, params=params)
        return res.json()

    def auto_getnmtoadd(self, id):
        """
        Список номенклатур для автоматической кампании

        https://openapi.wb.ru/promotion/api/ru/#tag/Upravlenie-parametrami-avtomaticheskih-kampanij/paths/~1adv~1v1~1auto~1getnmtoadd/get
        """

        params = {
            "id": id,
        }
        url = "https://advert-api.wb.ru/adv/v1/auto/getnmtoadd"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def auto_updatenm(self, id, add, delete):
        """
        Изменение списка номенклатур в автоматической кампании

        https://openapi.wb.ru/promotion/api/ru/#tag/Upravlenie-parametrami-avtomaticheskih-kampanij/paths/~1adv~1v1~1auto~1updatenm/post
        """

        params = {
            "id": id,
        }
        data = {
            "add": add,
            "delete": delete,
        }
        url = "https://advert-api.wb.ru/adv/v1/auto/updatenm"
        res = requests.post(url, headers=self.headers, json=data, params=params)
        return res.json()

    def auto_active(self, id, recom, booster, carousel):
        """
        Управление зонами показов в автоматической кампании

        https://openapi.wb.ru/promotion/api/ru/#tag/Upravlenie-parametrami-avtomaticheskih-kampanij/paths/~1adv~1v1~1auto~1active/post
        """

        params = {
            "id": id,
        }
        data = {
            "recom": recom,
            "booster": booster,
            "carousel": carousel,
        }
        url = "https://advert-api.wb.ru/adv/v1/auto/active"
        res = requests.post(url, headers=self.headers, json=data, params=params)
        return res.json()

    def auto_set_excluded(self, id, excluded):
        """
        Установка/удаление минус-фраз для автоматической кампании

        https://openapi.wb.ru/promotion/api/ru/#tag/Upravlenie-parametrami-avtomaticheskih-kampanij/paths/~1adv~1v1~1auto~1set-excluded/post
        """

        params = {
            "id": id,
        }
        data = {
            "excluded": excluded,
        }
        url = "https://advert-api.wb.ru/adv/v1/auto/set-excluded"
        res = requests.post(url, headers=self.headers, json=data, params=params)
        return res.json()

    def params_subject(self, id):
        """
        Словарь значений параметра subjectId

        https://openapi.wb.ru/promotion/api/ru/#tag/Slovari/paths/~1adv~1v0~1params~1subject/get
        """

        params = {
            "id": id,
        }
        url = "https://advert-api.Wildberries.ru/adv/v0/params/subject"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def params_menu(self, id):
        """
        Словарь значений параметра menuId

        https://openapi.wb.ru/promotion/api/ru/#tag/Slovari/paths/~1adv~1v0~1params~1menu/get
        """

        params = {
            "id": id,
        }
        url = "https://advert-api.Wildberries.ru/adv/v0/params/menu"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def params_set(self, id):
        """
        Словарь значений параметра setId

        https://openapi.wb.ru/promotion/api/ru/#tag/Slovari/paths/~1adv~1v0~1params~1set/get
        """

        params = {
            "id": id,
        }
        url = "https://advert-api.Wildberries.ru/adv/v0/params/set"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def supplier_subjects(self):
        """
        Предметы для кампаний

        https://openapi.wb.ru/promotion/api/ru/#tag/Slovari/paths/~1adv~1v1~1supplier~1subjects/get
        """

        url = "https://advert-api.Wildberries.ru/adv/v1/supplier/subjects"
        res = requests.get(url, headers=self.headers)
        return res.json()

    def supplier_nms(self, data_arr):
        """
        Номенклатуры для кампаний

        https://openapi.wb.ru/promotion/api/ru/#tag/Slovari/paths/~1adv~1v2~1supplier~1nms/post
        """

        url = "https://advert-api.wildberries.ru/adv/v2/supplier/nms"
        res = requests.post(url, headers=self.headers, json=data_arr)
        return res.json()

    def fullstats(self, id, interval_begin=None, interval_end=None, dates=None):
        """
        Статистика кампаний

        https://openapi.wb.ru/promotion/api/ru/#tag/Statistika/paths/~1adv~1v2~1fullstats/post
        """

        data = {
            "id": id,
        }
        if dates:
            data["dates"] = dates
        if interval_begin and interval_end:
            data["interval"] = {
                "begin": interval_begin,
                "end": interval_end,
            }
        url = "https://advert-api.wb.ru/adv/v2/fullstats"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def auto_stat(self, id):
        """
        Статистика автоматической кампании

        https://openapi.wb.ru/promotion/api/ru/#tag/Statistika/paths/~1adv~1v1~1auto~1stat/get
        """

        params = {
            "id": id,
        }
        url = "https://advert-api.wb.ru/adv/v1/auto/stat"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def auto_stat_words(self, id):
        """
        Статистика автоматической кампании по кластерам фраз

        https://openapi.wb.ru/promotion/api/ru/#tag/Statistika/paths/~1adv~1v2~1auto~1stat-words/get
        """

        params = {
            "id": id,
        }
        url = "https://advert-api.wb.ru/adv/v2/auto/stat-words"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def auto_daily_words(self, id):
        """
        Детальная статистика автоматической кампании по ключевым фразам

        https://openapi.wb.ru/promotion/api/ru/#tag/Statistika/paths/~1adv~1v2~1auto~1daily-words/get
        """

        params = {
            "id": id,
        }
        url = "https://advert-api.wb.ru/adv/v2/auto/daily-words"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def stat_words(self, id):
        """
        Статистика поисковой кампании по ключевым фразам

        https://openapi.wb.ru/promotion/api/ru/#tag/Statistika/paths/~1adv~1v1~1stat~1words/get
        """

        params = {
            "id": id,
        }
        url = "https://advert-api.wb.ru/adv/v1/stat/words"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def seacat_stat(self, id):
        """
        Статистика кампаний Поиск + Каталог

        https://openapi.wb.ru/promotion/api/ru/#tag/Statistika/paths/~1adv~1v1~1seacat~1stat/get
        """

        params = {
            "id": id,
        }
        url = "https://advert-api.wb.ru/adv/v1/seacat/stat"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()


class MediaAPI:
    """
    Работа с официальными методами API медиа продвижения

    https://openapi.wildberries.ru/promotion/api/ru/#tag/Prodvizhenie
    """

    def __init__(self, WB_TOKEN):
        self.WB_TOKEN = WB_TOKEN
        self.headers = {
            "Authorization": self.WB_TOKEN,
        }

    def count(self):
        """
        Получение медиакампаний

        https://openapi.wb.ru/promotion/api/ru/#tag/Media/paths/~1adv~1v1~1count/get
        """

        url = "https://advert-media-api.wb.ru/adv/v1/count"
        res = requests.get(url, headers=self.headers)
        return res.json()

    def adverts(
        self,
        status=None,
        type=None,
        limit=None,
        offset=None,
        order=None,
        direction=None,
    ):
        """
        Список медиакампаний

        https://openapi.wb.ru/promotion/api/ru/#tag/Media/paths/~1adv~1v1~1adverts/get
        """

        params = {}
        if status:
            params["status"] = status
        if type:
            params["type"] = type
        if limit:
            params["limit"] = limit
        if offset:
            params["offset"] = offset
        if order:
            params["order"] = order
        if direction:
            params["status"] = direction
        url = "https://advert-media-api.wb.ru/adv/v1/adverts"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def advert(self, id):
        """
        Информация о медиакампании

        https://openapi.wb.ru/promotion/api/ru/#tag/Media/paths/~1adv~1v1~1advert/get
        """

        params = {
            "id": id,
        }
        url = "https://advert-media-api.wb.ru/adv/v1/advert"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def advert_stop(self, advert_id, reason):
        """
        Завершение медиакампании

        https://openapi.wb.ru/promotion/api/ru/#tag/Aktivnost-mediakampanii/paths/~1adv~1v1~1advert~1stop/post
        """

        data = {
            "advert_id": advert_id,
        }
        if reason:
            data["reason"] = reason
        url = "https://advert-media-api.wb.ru/adv/v1/advert/stop"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def advert_pause(self, advert_id, reason):
        """
        Приостановка медиакампании

        https://openapi.wb.ru/promotion/api/ru/#tag/Aktivnost-mediakampanii/paths/~1adv~1v1~1advert~1pause/post
        """

        data = {
            "advert_id": advert_id,
        }
        if reason:
            data["reason"] = reason
        url = "https://advert-media-api.wb.ru/adv/v1/advert/pause"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def advert_start(self, advert_id, reason):
        """
        Запуск медиакампании

        https://openapi.wb.ru/promotion/api/ru/#tag/Aktivnost-mediakampanii/paths/~1adv~1v1~1advert~1start/post
        """

        data = {
            "advert_id": advert_id,
        }
        if reason:
            data["reason"] = reason
        url = "https://advert-media-api.wb.ru/adv/v1/advert/start"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def stats(self, id, interval_begin=None, interval_end=None, dates=None):
        """
        Статистика медиакампаний

        https://openapi.wb.ru/promotion/api/ru/#tag/Statistika-mediakampanii/paths/~1adv~1v1~1stats/post
        """

        data = {
            "id": id,
        }
        if dates:
            data["dates"] = dates
        if interval_begin and interval_end:
            data["interval"] = {
                "begin": interval_begin,
                "end": interval_end,
            }
        url = "https://advert-media-api.wb.ru/adv/v1/stats"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def item_cpm_change(self, advert_id, item_id, cpm):
        """
        Изменение ставки баннера

        https://openapi.wb.ru/promotion/api/ru/#tag/Stavki-mediakampanii/paths/~1adv~1v1~1item~1cpm~1change/post
        """

        data = {
            "advert_id": advert_id,
            "item_id": item_id,
            "cpm": cpm,
        }
        url = "https://advert-media-api.wb.ru/adv/v1/item/cpm/change"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()
