import requests


class RecommendationsAPI:
    """
    Класс для работы с рекоммендациями

    https://openapi.wildberries.ru/recommendations/api/ru/#tag/Rekomendacii
    """

    def __init__(self, wb_token):
        self.wb_token = wb_token
        self.headers = {
            "Authorization": self.wb_token,
            "Content type": "application/json",
        }

    def ins(self, data_arr):
        """
        Добавление рекомендаций

        https://openapi.wildberries.ru/recommendations/api/ru/#tag/Rekomendacii/paths/~1api~1v1~1ins/post
        """

        url = "https://recommend-api.wildberries.ru/api/v1/ins"
        res = requests.post(url, headers=self.headers, json=data_arr)
        return res.json()

    def delete(self, data_arr):
        """
        Удаление рекомендаций

        https://openapi.wildberries.ru/recommendations/api/ru/#tag/Rekomendacii/paths/~1api~1v1~1del/post
        """

        url = "https://recommend-api.wildberries.ru/api/v1/del"
        res = requests.post(url, headers=self.headers, json=data_arr)
        return res.json()

    def set(self, data_arr):
        """
        Управление рекомендациями

        https://openapi.wildberries.ru/recommendations/api/ru/#tag/Rekomendacii/paths/~1api~1v1~1set/post
        """

        url = "https://recommend-api.wildberries.ru/api/v1/set"
        res = requests.post(url, headers=self.headers, json=data_arr)
        return res.json()

    def list(self, data_arr, limit=None):
        """
        Список рекомендаций

        https://openapi.wildberries.ru/recommendations/api/ru/#tag/Rekomendacii/paths/~1api~1v1~1list/post
        """

        params = {}
        if limit:
            params["limit"] = limit
        url = "https://recommend-api.wildberries.ru/api/v1/list"
        res = requests.post(url, headers=self.headers, json=data_arr, params=params)
        return res.json()
