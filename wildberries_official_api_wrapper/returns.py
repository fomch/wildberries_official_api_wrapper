import requests


class ReturnsAPI:
    """
    Класс для работы с возвратами покупателей

    https://openapi.wb.ru/returns/api/ru/
    """

    def __init__(self, wb_token):
        self.wb_token = wb_token
        self.headers = {
            "Authorization": self.wb_token,
            "Content type": "application/json",
        }

    def get_claims(self, is_archive, id=None, limit=None, offset=None, nm_id=None):
        """
        Заявки покупателей на возврат

        https://openapi.wb.ru/returns/api/ru/#tag/Vozvraty-pokupatelyami/paths/~1api~1v1~1claims/get
        """

        params = {
            "is_archive": is_archive,
        }
        if id:
            params["id"] = id
        if limit:
            params["limit"] = limit
        if offset:
            params["offset"] = offset
        if nm_id:
            params["nm_id"] = nm_id
        url = "https://returns-api.wildberries.ru/api/v1/claims"
        res = requests.get(url, params=params, headers=self.headers)
        return res.json()

    def patch_claim(self, id, action):
        """
        Ответ на заявку покупателя

        https://openapi.wb.ru/returns/api/ru/#tag/Vozvraty-pokupatelyami/paths/~1api~1v1~1claim/patch
        """

        data = {
            "id": id,
            "action": action,
        }
        url = "https://returns-api.wildberries.ru/api/v1/claim"
        res = requests.patch(url, headers=self.headers, json=data)
        return res.json()
