import time

import requests
from deprecated import deprecated


class StatisticAPI:
    """
    Класс для работы со Статистикой

    https://openapi.wildberries.ru/statistics/api/ru/#tag/Statistika
    """

    def __init__(self, wb_token):
        self.wb_token = wb_token
        self.headers = {
            "Authorization": self.wb_token,
            "Content type": "application/json",
        }

    def get_incomes(self, dateFrom):
        """
        https://openapi.wildberries.ru/statistics/api/ru/#tag/Statistika/paths/~1api~1v1~1supplier~1incomes/get
        """

        params = {
            "dateFrom": dateFrom,
        }
        url = "https://statistics-api.wildberries.ru/api/v1/supplier/incomes"
        res = requests.get(url, params=params, headers=self.headers)
        return res.json()

    def get_stocks_deprecated(self, dateFrom):
        """
        https://openapi.wildberries.ru/statistics/api/ru/#tag/Statistika/paths/~1api~1v1~1supplier~1stocks/get
        """

        params = {
            "dateFrom": dateFrom,
        }
        url = "https://statistics-api.wildberries.ru/api/v1/supplier/stocks"
        res = requests.get(url, params=params, headers=self.headers)
        return res.json()

    def get_stocks(self, dateFrom=None, params=None, sleep_duration=5):
        """
        https://dev.wildberries.ru/openapi/reports#tag/Otchyot-po-ostatkam-na-skladah
        """

        url = "https://seller-analytics-api.wildberries.ru/api/v1/warehouse_remains"
        res = requests.get(url, params=params, headers=self.headers).json()
        task_id = res["data"]["taskId"]

        while True:
            url = f"https://seller-analytics-api.wildberries.ru/api/v1/warehouse_remains/tasks/{task_id}/status"
            res1 = requests.get(url, headers=self.headers).json()
            if "data" in res and res1["data"]["status"] == "done":
                break
            time.sleep(sleep_duration)

        url = f"https://seller-analytics-api.wildberries.ru/api/v1/warehouse_remains/tasks/{task_id}/download"
        res = requests.get(url, params=params, headers=self.headers).json()
        return res

    def get_orders(self, dateFrom):
        """
        https://openapi.wildberries.ru/statistics/api/ru/#tag/Statistika/paths/~1api~1v1~1supplier~1orders/get
        """

        params = {
            "dateFrom": dateFrom,
            "flag": 0,
        }
        url = "https://statistics-api.wildberries.ru/api/v1/supplier/orders"
        res = requests.get(url, params=params, headers=self.headers)
        return res.json()

    def get_sales(self, dateFrom):
        """
        https://openapi.wildberries.ru/statistics/api/ru/#tag/Statistika/paths/~1api~1v1~1supplier~1sales/get
        """

        params = {
            "dateFrom": dateFrom,
        }
        url = "https://statistics-api.wildberries.ru/api/v1/supplier/sales"
        res = requests.get(url, params=params, headers=self.headers)
        return res.json()

    def reportDetailByPeriod(self, dateFrom, dateTo, limit=100000, rrdid=0, v="v5"):
        """
        https://openapi.wildberries.ru/statistics/api/ru/#tag/Statistika/paths/~1api~1v1~1supplier~1reportDetailByPeriod/get
        """

        params = {
            "dateFrom": dateFrom,
            "dateTo": dateTo,
            "limit": limit,
            "rrdid": rrdid,
        }
        url = f"https://statistics-api.wildberries.ru/api/{v}/supplier/reportDetailByPeriod"
        res = requests.get(url, params=params, headers=self.headers)
        return res.json()


class DelayedGenTasksAPI:
    """
    DEPRECATED
    Класс для работы с платным хранением

    https://openapi.wb.ru/statistics/api/ru/#tag/Otchyoty
    """

    def __init__(self, wb_token):
        self.wb_token = wb_token
        self.headers = {
            "Authorization": self.wb_token,
            "Content type": "application/json",
        }

    @deprecated(reason="Метод устарел и скоро будет удален")
    def create(self, dateFrom, dateTo, _type="paid_storage"):
        """
        https://openapi.wildberries.ru/statistics/api/ru/#tag/Poluchenie-otchyotov/paths/~1api~1v1~1delayed-gen~1tasks~1create/post
        """

        data = {
            "type": _type,
            "params": {
                "dateFrom": dateFrom,
                "dateTo": dateTo,
            },
        }
        url = "https://statistics-api.wildberries.ru/api/v1/delayed-gen/tasks/create"
        res = requests.post(url, headers=self.headers, json=data)
        # print(res.status_code)
        return res.json()

    @deprecated(reason="Метод устарел и скоро будет удален")
    def status(self, id):
        """
        https://openapi.wildberries.ru/statistics/api/ru/#tag/Poluchenie-otchyotov/paths/~1api~1v1~1delayed-gen~1tasks/get
        """

        data = {
            "ids": [id],
        }
        url = "https://statistics-api.wildberries.ru/api/v1/delayed-gen/tasks"
        res = requests.get(url, headers=self.headers, json=data)
        # print(res.status_code)
        return res.json()

    @deprecated(reason="Метод устарел и скоро будет удален")
    def download(self, id):
        """
        https://openapi.wildberries.ru/statistics/api/ru/#tag/Poluchenie-otchyotov/paths/~1api~1v1~1delayed-gen~1tasks~1download/get
        """

        data = {
            "id": id,
        }
        url = "https://statistics-api.wildberries.ru/api/v1/delayed-gen/tasks/download"
        res = requests.get(url, headers=self.headers, json=data)
        # print(res.status_code)
        return res.json()
