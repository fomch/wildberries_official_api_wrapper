import requests


class SuppliesAPI:
    """
    Класс для работы с поставками

    https://openapi.wb.ru/supplies/api/ru/
    """

    def __init__(self, wb_token):
        self.wb_token = wb_token
        self.headers = {
            "Authorization": self.wb_token,
            "Content type": "application/json",
        }

    def acceptance_coefficients(self, warehouseIDs):
        """
        Коэффициенты приёмки

        https://openapi.wb.ru/supplies/api/ru/#tag/Informaciya-dlya-formirovaniya-postavok/paths/~1api~1v1~1acceptance~1coefficients/get
        """

        params = {}
        if warehouseIDs:
            params["warehouseIDs"] = warehouseIDs
        url = "https://supplies-api.wildberries.ru/api/v1/acceptance/coefficients"
        res = requests.get(url, headers=self.headers, params=params)
        return res.json()

    def acceptance_options(self, data):
        """
        Опции приёмки

        https://openapi.wb.ru/supplies/api/ru/#tag/Informaciya-dlya-formirovaniya-postavok/paths/~1api~1v1~1acceptance~1options/post
        """

        url = "https://supplies-api.wildberries.ru/api/v1/acceptance/options"
        res = requests.post(url, headers=self.headers, json=data)
        return res.json()

    def warehouses(self):
        """
        Список складов

        https://openapi.wb.ru/supplies/api/ru/#tag/Informaciya-dlya-formirovaniya-postavok/paths/~1api~1v1~1warehouses/get
        """

        url = "https://supplies-api.wildberries.ru/api/v1/warehouses"
        res = requests.get(url, headers=self.headers)
        return res.json()
