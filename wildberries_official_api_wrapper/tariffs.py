import requests


class TariffsAPI:
    """
    Класс для работы с тарифами

    https://openapi.wb.ru/prices/api/ru/#tag/Ceny
    """

    def __init__(self, wb_token):
        self.wb_token = wb_token
        self.headers = {
            "Authorization": self.wb_token,
            "Content type": "application/json",
        }

    def tariffs_commission(self, locale="ru"):
        """
        Комиссия по категориям товаров
        """

        params = {
            "locale": locale,
        }
        url = "https://common-api.wildberries.ru/api/v1/tariffs/commission"
        res = requests.get(url, params=params, headers=self.headers)
        return res.json()

    def tariffs_box(self, date):
        """
        Тарифы для коробов

        https://openapi.wildberries.ru/tariffs/api/ru/#tag/Koefficienty-skladov/paths/~1api~1v1~1tariffs~1box/get
        """

        params = {
            "date": date,
        }
        url = "https://common-api.wildberries.ru/api/v1/tariffs/box"
        res = requests.get(url, params=params, headers=self.headers)
        return res.json()

    def tariffs_pallet(self, date):
        """
        Тарифы для монопалет

        https://openapi.wildberries.ru/tariffs/api/ru/#tag/Koefficienty-skladov/paths/~1api~1v1~1tariffs~1pallet/get
        """

        params = {
            "date": date,
        }
        url = "https://common-api.wildberries.ru/api/v1/tariffs/pallet"
        res = requests.get(url, params=params, headers=self.headers)
        return res.json()

    def tariff_return(self, date):
        """
        Тарифы на возврат

        https://openapi.wildberries.ru/tariffs/api/ru/#tag/Stoimost-vozvrata-prodavcu/paths/~1api~1v1~1tariffs~1return/get
        """

        params = {
            "date": date,
        }
        url = "https://common-api.wildberries.ru/api/v1/tariffs/return"
        res = requests.get(url, params=params, headers=self.headers)
        return res.json()
